import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by levineugene on 2/2/14.
 */
public class PercolationTest {
    @Test
    public void testcalculateCoordsToIdx() throws Exception {
        final int N = 10;
        Percolation Percolation = null;

        assertEquals(0, Percolation.calculatecCoordsToIdx(N, 1, 1));
        assertEquals(1, Percolation.calculatecCoordsToIdx(N, 1, 2));
        assertEquals(N - 1, Percolation.calculatecCoordsToIdx(N, 1, N));
        assertEquals(N, Percolation.calculatecCoordsToIdx(N, 2, 1));
        assertEquals(N + 1, Percolation.calculatecCoordsToIdx(N, 2, 2));
        assertEquals(N + N - 1, Percolation.calculatecCoordsToIdx(N, 2, N));
    }
}
