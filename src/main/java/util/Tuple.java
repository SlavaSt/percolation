package util;

/**
 * Created by levineugene on 2/2/14.
 */
public class Tuple<T1, T2> {

    private final T1 obj1;
    private final T2 obj2;

    public Tuple(T1 obj1, T2 obj2) {
        if (obj1 == null || obj2 == null) {
            throw new IllegalArgumentException("Tuple doesn't accept null arguments.");
        }
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    protected T1 getObj1() {
        return obj1;
    }

    protected T2 getObj2() {
        return obj2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tuple)) return false;

        Tuple tuple = (Tuple) o;

        if (!obj1.equals(tuple.obj1)) return false;
        if (!obj2.equals(tuple.obj2)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = obj1.hashCode();
        result = 31 * result + obj2.hashCode();
        return result;
    }
}
