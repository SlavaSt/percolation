package util;


/**
 * @author Slava Stashuk
 */
public class KeyValuePrinter {

    private final Tuple[] keyValues;

    public KeyValuePrinter() {
        this(new Tuple[0]);
    }

    private KeyValuePrinter(Tuple[] keyValues) {
        this.keyValues = keyValues;
    }

    public KeyValuePrinter add(Object key, Object value) {
        Tuple[] keyValues = new Tuple[this.keyValues.length + 1];
        System.arraycopy(this.keyValues, 0, keyValues, 0, this.keyValues.length);
        keyValues[keyValues.length - 1] = new Tuple(key, value);
        return new KeyValuePrinter(keyValues);
    }

    public String mkString() {
        int maxLen = 0;
        for (Tuple keyValue : keyValues) {
            int len = keyValue.getObj1().toString().length();
            if (maxLen < len)
                maxLen = len;
        }
        String res = "";
        for (Tuple keyValue : keyValues) {
            res += String.format("%-" + maxLen + "s = %s", keyValue.getObj1(), keyValue.getObj2()) +
                    System.lineSeparator();
        }
        return res;
    }

    private static class Tuple extends util.Tuple<Object, Object> {
        public Tuple(Object obj1, Object obj2) {
            super(obj1, obj2);
        }
    }

}
