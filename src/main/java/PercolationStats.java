
import util.KeyValuePrinter;

import static java.lang.Math.sqrt;

/**
 * @author Slava Stashuk
 */
public class PercolationStats {

    private final double[] x;

    private final int T;

    public PercolationStats(int N, int T) {    // perform T independent computational experiments on an N-by-N grid
        this.T = T;
        x = new double[T];
        for (int i = 0; i < T; i++) {
            Experiment experiment = new Experiment(N);
            experiment.run();
            x[i] = experiment.openSitesFraction();
        }
    }

    public double mean() {                    // sample mean of percolation threshold
        double res = 0;
        for (int i = 0; i < T; i++) res += x[i];
        return res / T;
    }

    public double stddev() {                  // sample standard deviation of percolation threshold
        double mean = mean();
        double res = 0;
        for (int i = 0; i < T; i++) res += pow2(x[i] - mean);
        return Math.sqrt(res / (T - 1));
    }

    public double confidenceLo() {             // returns lower bound of the 95% confidence interval
        return mean() - 1.96 * stddev() / sqrt(T);
    }

    public double confidenceHi() {             // returns upper bound of the 95% confidence interval
        return mean() + 1.96 * stddev() / sqrt(T);
    }

    private static double pow2(double val) {
        return val * val;
    }

    /**
     * % java PercolationStats 200 100
     * mean                    = 0.5929934999999997
     * stddev                  = 0.00876990421552567
     * 95% confidence interval = 0.5912745987737567, 0.5947124012262428
     *
     * @param args Ignores the args
     */
    public static void main(String[] args) {   // test client, described below
        if (args.length != 2) {
            System.out.println(usage());
            return;
        }
        int N, T;
        try {
            N = Integer.parseInt(args[0]);
            T = Integer.parseInt(args[1]);
        } catch (NumberFormatException exc) {
            System.out.println(usage());
            return;
        }

        PercolationStats stats = new PercolationStats(N, T);
        String output = new KeyValuePrinter()
                .add("mean", stats.mean())
                .add("stddev", stats.stddev())
                .add("95% confidence interval", stats.confidenceLo() + ", " + stats.confidenceHi())
                .mkString();
        System.out.println(output);
    }

    private static String usage() {
        return "[USAGE] % java PercolationStats 200 100\n" +
                "mean                    = 0.5929934999999997\n" +
                "stddev                  = 0.00876990421552567\n" +
                "95% confidence interval = 0.5912745987737567, 0.5947124012262428";
    }
}
