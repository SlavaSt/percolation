/**
* Created by levineugene on 2/2/14.
*/
class Experiment {

    int openedCount = 0;
    final Coord[] coords;
    final Percolation percolation;
    final int N;

    Experiment(int N) {
        this.N = N;
        coords = new Coord[N * N];
        percolation = new Percolation(N);
        int counter = 0;
        for (int i=1; i<=N; i++) {
            for (int j=1; j<=N; j++) {
                coords[counter++] = new Coord(i, j);
            }
        }
        StdRandom.shuffle(coords);
    }

    public void run() {
        while (!percolation.percolates())
            openNext();
    }

    public double openSitesFraction() {
        return openedCount * 1.0d / (N * N);
    }

    private void openNext() {
        Coord coordToOpen = coords[openedCount++];
        percolation.open(coordToOpen.x(), coordToOpen.y());
    }

}
