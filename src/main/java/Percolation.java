/**
 * Created by levineugene on 2/2/14.
 */
public class Percolation {

    private final WeightedQuickUnionUF UF;

    private final boolean[][] grid;

    private final int N;

    private final int topVirtualIdx;

    private final int bottomVirtualIdx;

    /**
     * create N-by-N UF, with all sites blocked
     * @param N
     */
    public Percolation(int N) {
        this.N = N;
        topVirtualIdx = N*N;
        bottomVirtualIdx = N*N + 1;
        grid = new boolean[N][N];
        UF = new WeightedQuickUnionUF(N*N + 2);
        for (int i = 1; i <= N; i++) {
            UF.union(topVirtualIdx, coordsToIdx(1, i));
            UF.union(bottomVirtualIdx, coordsToIdx(N, i));
        }
    }

    /**
     * open site (row i, column j) if it is not already
     * @param i
     * @param j
     */
    public void open(int i, int j) {
        grid[i - 1][j - 1] = true;
        int idx = coordsToIdx(i, j);
        if (safeIsOpen(i - 1, j)) UF.union(idx, coordsToIdx(i - 1, j));
        if (safeIsOpen(i + 1, j)) UF.union(idx, coordsToIdx(i + 1, j));
        if (safeIsOpen(i, j - 1)) UF.union(idx, coordsToIdx(i, j - 1));
        if (safeIsOpen(i, j + 1)) UF.union(idx, coordsToIdx(i, j + 1));
    }

    /**
     * is site (row i, column j) open?
     * @param i
     * @param j
     * @return
     */
    public boolean isOpen(int i, int j) {
        return grid[i-1][j-1];
    }

    private boolean safeIsOpen(int i, int j) {
        return (i < 1 || i > N || j < 1 || j > N) ? false : isOpen(i, j);
    }

    /**
     * is site (row i, column j) full?
     * @param i
     * @param j
     * @return
     */
    public boolean isFull(int i, int j) {
        return UF.connected(topVirtualIdx, coordsToIdx(i, j));
    }

    /**
     * does the system percolate?
     * @return
     */
    public boolean percolates() {
        return UF.connected(topVirtualIdx, bottomVirtualIdx);
    }

    private int coordsToIdx(int i, int j) {
        return calculatecCoordsToIdx(N, i, j);
    }

    static int calculatecCoordsToIdx(int N, int i, int j) {
        return N*(i - 1) + j - 1;
    }
}
