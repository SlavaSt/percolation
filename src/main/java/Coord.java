import util.Tuple;

/**
* Created by levineugene on 2/2/14.
*/
class Coord extends Tuple<Integer, Integer> {
    public Coord(Integer obj1, Integer obj2) {
        super(obj1, obj2);
    }

    public int x() {
        return getObj1();
    }

    public int y() {
        return getObj2();
    }
}
